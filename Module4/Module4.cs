﻿using System;
using System.Linq;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {

        }

        public int Task_1_A(int[] array)
        {
            return array.Max();
        }

        public int Task_1_B(int[] array)
        {
            return array.Min();
        }

        public int Task_1_C(int[] array)
        {
            return array.Sum();
        }

        public int Task_1_D(int[] array)
        {
            return array.Max() - array.Min() ;
        }

        public void Task_1_E(int[] array)
        {
            int max = array.Max();
            int min = array.Min();

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] % 2 == 0)
                {
                    array[i] += max;
                }
                else
                {
                    array[i] += min;
                }
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return a + b;
        }

        public int[] Task_2(int[] a, int[] b)
        {

            bool firstMax = (a.Length > b.Length) ? true : false;
            int maxLength = firstMax ? a.Length : b.Length;
            int minLength = firstMax ? b.Length : a.Length;
            int[] result = (int[])(firstMax ? a : b).Clone();
            for (int i = 0; i < result.Length; i++)
            {
                if (i >= minLength) break;
                result[i] = a[i] + b[i];
            }
            return result;

        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius > 0)
            {
                double pi = 3.14d;
                length = 2 * pi * radius;
                square = pi * (radius * radius);
            } else
            {
                throw new ArgumentException();
            }
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            maxItem = array.Max();
            minItem = array.Min();
            sumOfItems = array.Sum();
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            var tuple = numbers;
            tuple.Item1 += 10;
            tuple.Item2 += 10;
            tuple.Item3 += 10;
            return tuple;
        }

        public (double, double) Task_4_B(double radius)
        {
            if(radius > 0)
            {
                double pi = 3.14d;
                var length = 2 * pi * radius;
                var square = pi * (radius * radius);
                var tuple = (length, square);
                return tuple;
            } else
            {
                throw new ArgumentNullException();
            }
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            if(array == null && array.Length == 0)
            {
                throw new ArgumentNullException();
            }
            else
            {
                var sum = array.Sum();
                var min = array.Min();
                var max = array.Max();
                var tuple = (min, max, sum);
                return tuple;

            }
        }

        public void Task_5(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] += 5;
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (direction == SortDirection.Ascending && array.Length > 1)
            {
                Array.Sort(array);
            }
            else if (direction == SortDirection.Descending && array.Length > 1)
            {
                Array.Reverse(array);
            }
        }

        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            while (x2 - x1 > e)
            {
                double XAverage = (x1 + x2) / 2;
                if (func(XAverage) * func(x1) > 0)
                {
                    result = x1;
                    return Task_7(func, XAverage, x2, e, result);
                }
                else
                {
                    return Task_7(func, x1, XAverage, e, result);
                }
            }
            return result;
        }
    }
}
